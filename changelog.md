# Changelog
### 1.0
* Updated mod for Avorion 2.*

### 0.3.7
* Updated mod for Avorion 1.*
* Removed timestamp from logging interface, because Avorion added it to every log entry

### 0.3.6
* Fixed `ConfigLib.getLoglevel()` not working

### 0.3.5
* Fixed an issue that lead to client not creating config directory

### 0.3.4
* Fixed a possible client crash
* Temporarily removed usage of loglevel config file

### 0.3.3
* Added config file for loglevel (dev mode will still force loglevel >= 4)

### 0.3.2
* Added timestamps to logs

### 0.3.1
* Fixed an issue with numbers in config files 

### 0.3.0
* Reworked the whole library to solve a lot of instancing and client/server communication issues

Be ware that this version changes usage of the mod. Please watch the [wiki pages](https://gitlab.com/hams-avorion-projects/configlib/wikis/home) for more information.

### 0.2.2
* Add table support to mod configs (only numeric indices, multidimensions supported)

### 0.2.1
* Client side will also work now, when the calling file does not have a namespace


### 0.2.0
* Alpha release