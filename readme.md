# ConfigLib

A resource for modders, to provide galaxy related mod configurations for your own mods. Config files are stored on server side, while client will automaticly fetch it from server.

Beside this it provides a logging tool you can optionally use in your mods.

## Installation

This mod is available in Steam Workshop and on GitLab. I recommand subscribing on Steam.

## Wiki
For usage instructions you should watch the [wiki pages](https://gitlab.com/hams-avorion-projects/configlib/wikis/home).


#### About logging lib

Beside the main part of this mod, configs, this mod provides a simple logging library, this way you do not have to manually load both in your scripts. Other logging libs may be better then this one. My intention is not to rival for the most common logging lib or so, its just for having both in one package.

When you have a logging lib you/we may create an adapter mod wich makes the ConfigLib use your logging lib. Feel free to contact me for this.