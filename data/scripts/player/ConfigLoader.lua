package.path = package.path .. ";data/scripts/lib/?.lua"
package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"

local ConfigIO = include("ConfigIO")

include("callable")
include("utility")


-- Don't remove or alter the following comment, it tells the game the namespace this script lives in. If you remove it, the script will break.
-- namespace ConfigLoader
ConfigLoader = {}

-- onClient Start --
if onClient() then
function ConfigLoader.initialize()
	createDirectory("moddata/ConfigLib")
	
	invokeServerFunction("sendConfig", Player().index)
end
end
-- onClient End --



function ConfigLoader.sendConfig(playerIndex)
	local config = {}
	local player = Player(playerIndex)
	
	local mods = include("ConfigLibRegistration")
	
	if player and mods then
		for _,modID in pairs(mods) do
			-- config[modID] = modconfig -- pseudocode!
			config[modID] = ConfigIO.getModConfig(modID)
		end
	end
	
	invokeClientFunction(player, "receiveConfig", config)
end
callable(ConfigLoader, "sendConfig")



function ConfigLoader.receiveConfig(config)
	for modID,modConfig in pairs(config) do
		ConfigIO.writeClientConfig(modID, modConfig)
	end
end



