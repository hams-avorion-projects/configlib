local registeredMods = {}

local function addMod(modID)
	if type(modID) ~= "string" or modID == "" then print("[CONFIG LIB] Invalid modID in ConfigLibRegistration.lua. ID must be type of string and not be empty.", type(modID), "given") end
	
    table.insert(registeredMods, modID)
end

addMod("1720259598")

return registeredMods