
local ConfigIO = {}


function ConfigIO.parseFileContents(tblData)
	local ret = "return {\n"
	
	for key,row in pairs(tblData) do
		ret = ret .. ConfigIO.parseConfigRow(key,row)
	end
	
	ret = ret .. "}"
	
	return ret
end

function ConfigIO.parseConfigRow(key,row)
	local rowKey
	if onServer() then
		rowKey = row.name .. " = "
	else
		rowKey = key .. " = "
	end
	rowKey = ConfigIO.formatString(rowKey)
	
	local rowValue = row.value
	rowValue = ConfigIO.varToLua(rowValue)
	rowValue = 'value = ' .. rowValue .. ','
	rowValue = ConfigIO.formatString(rowValue)
	
	if onServer() then
		return "    " .. rowKey .. '{ ' .. rowValue .. ' description = "' .. (row.description or "") .. '", sorting = ' .. key .. ' },\n'
	else
		return "    " .. rowKey .. '{ ' .. rowValue .. ' description = "' .. (row.description or "") .. '"},\n'
	end
end

-- Parse multiple data types to lua sourcecode (string)
function ConfigIO.varToLua(value, level)
	if type(value) == "string" then
		value = '"' .. value .. '"'
	elseif(type(value) == "number") then
		-- use raw value
	elseif(type(value) == "boolean") then
		if value then
			value = "true"
		else
			value = "false"
		end
	elseif(type(value) == "table") then
		local tbl = value
		level = level or 1
		local space = string.rep(" ", 4 * level)
		
		value = "{\n    " .. space .. "-- Add table rows below this line"
		for _,v in ipairs(tbl) do
			value = value .. "\n    " .. space .. ConfigIO.varToLua(v, level + 1) .. ","
		end
		value = value .. "\n" .. space .. "}"
	else
		-- Fallback, invalid type. Return empty string
		print("[Config Lib] Tried to parse invalid type to lua code:", type(value))
		value = '""'
	end
	
	return value
end

function ConfigIO.formatString(str, length)
	length = tonumber(length) or 24
	while str:len() < length do -- some ugly thing, maybe use a string format instead?
		str = str .. " "
	end
	
	-- Some more nice style
	-- Todo: test and update
	--local oldlen = str:len()
	--str = str .. string.rep(" ",  tonumber(length) or 24)
	--print(str:len()) -- prints (oldlen + (tonumber(length) or 24))
	
	return str
end

-- Read config file
-- For reading original config do 'include("filename")' instead
function ConfigIO.read(strFilename)
	local ResConfigFile, StrError = io.open(strFilename, "r")
	
	if ResConfigFile then -- if file exist
		local srcConfig = ResConfigFile:read("*all")
		ResConfigFile:close()
		
		return loadstring(srcConfig)() 
	end
	
	return {}
end


-- Write table to file
-- Create file if it does not exist
-- Delete all contents if it already exist
function ConfigIO.write(strFilename, tblData)
	local StrFile = ConfigIO.parseFileContents(tblData)
	
	local ResFile, StrError = io.open(strFilename, "w+")
	if ResFile then
		ResFile:write(StrFile)
		ResFile:close()
	end
end


-- Merge 2 tables and write to config file. Keep variables and order of tblOriginal, insert values of tblModded if given
function ConfigIO.merge(strFilename, tblOriginal, tblModded)
	local tblMerged = tblOriginal
	
	for i,row in pairs(tblOriginal) do
		if tblModded[row.name] then
			tblMerged[i].value = tblModded[row.name].value
		end
	end
	
	ConfigIO.write(strFilename, tblMerged)
end

-- Get the filename of the given mod without ".lua" extension.
function ConfigIO.getFilename(modID, ModName)
	if not modID then return end
	
	local mod = ConfigIO.getMod(modID)
	if not mod then return end
	
	if not ModName then
		ModName = mod.name
	end
	
	return ModName .. "-" .. modID
end

-- Get the modconfig path.
function ConfigIO.getConfigPath()
	if onServer() then -- Get config path inside galaxy folder
		return Server().folder .. "/moddata/ConfigLib/"
	else -- Get config path in userdata folder
		return "ConfigLib/"
	end
end

-- Get the full mod config path with filename and type extension (.lua)
function ConfigIO.getFullConfigPath(modID)
	local path = ConfigIO.getConfigPath()
	
	if onClient() then
		path = "moddata/" .. path -- getConfigPath() does not add moddata folder for client
	end
	local filename = ConfigIO.getFilename(modID)
	
	if path and filename then
		return path .. filename .. ".lua"
	end
	
	return ""
end

-- Get the original modconfig path.
function ConfigIO.getOrigConfigPath()
	return "data/scripts/modconfig/"
end


function ConfigIO.getMod(modID)
	local manager = ModManager()
	local mod = manager:findEnabled(modID)
	
	if not mod then
		print("[Config Lib] Could not load mod by id:", modID)
	end
	
	return mod
end


---------------------------------
------- Public namespace --------
---------------------------------
local PublicNamespace = {}

-- Load metadata for given mod
function PublicNamespace.getMod(modID)
	return ConfigIO.getMod(modID)
end

-- Load mod configs for given mods
function PublicNamespace.getModConfig(modID)
	local filename = ConfigIO.getFullConfigPath(modID)
	return ConfigIO.read(filename)
end

-- Update the server sided config file. Create if it does not exist yet
function PublicNamespace.updateModConfig(modID, origConfig)
	if not onServer() or not modID then return end
	local mod = ConfigIO.getMod(modID)
	
	if not mod then return end
	
	local filename = ConfigIO.getFilename(modID, mod.name)
	local path = ConfigIO.getConfigPath()
	
	if not origConfig then
		origConfig = include(ConfigIO.getOrigConfigPath() .. filename)
	end
	
	if origConfig then
		local modConfig = ConfigIO.read(path .. filename .. ".lua")
		
		if not modConfig then return end
		
		ConfigIO.merge(path .. filename .. ".lua", origConfig, modConfig)
	end
end

-- Write a temporary client sided config
function PublicNamespace.writeClientConfig(modID, modConfig)
	if not onClient() or not modConfig then return end
	
	local filename = ConfigIO.getFullConfigPath(modID)
	ConfigIO.write(filename, modConfig)
end


return PublicNamespace


