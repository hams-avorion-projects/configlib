package.path = package.path .. ";data/scripts/lib/?.lua"
package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"
--package.path = package.path .. ";data/scripts/modconfig/?.lua" -- Should not be required?

include("utility")

local ConfigIO = include("ConfigIO")

-- Create namespaces
local ConfigLib = {} -- Public namespace

local PrivateNamespace = {}
PrivateNamespace.__index = PrivateNamespace

-- Loglevel - not implemented yet
local loglevels = {
	FATAL = 1,
	ERROR = 2,
	WARNING = 3,
	DEBUG = 4,
	NOTICE = 5
}


-- Create new mod config insatance
local function new()
	local Instance = {}
	
	Instance.ModConfig = {}				-- The user configurations of the mod
	Instance.Metadata = {}				-- The metadata (Steam config) of the mod
	
	Instance.logPrefix = ""
	
	-- We also need configs and logging for this mod itself
	-- All functions and variables will have a "Self" prefix
	Instance.SelfMetadata = ConfigIO.getMod("1720259598")
	Instance.SelfLogPrefix = ""
	-- Instance.SelfConfig = ConfigIO.getModConfig("1720259598")
	
	-- Loglevel - a simplified implementation
	-- Instance.logLevel = Instance.SelfConfig.loglevel or 1
	Instance.logLevel = 1
	
	if GameSettings().devMode and Instance.logLevel < 4 then
		Instance.logLevel = 4
	end
	
	return setmetatable(Instance, PrivateNamespace)
end


-------------------------------
-- Private namespace ----------
-------------------------------
function PrivateNamespace:getMetadata(modId)
	return ConfigIO.getMod(modId)
end


function PrivateNamespace:loadModConfig(modID)
	return ConfigIO.getModConfig(modID)
end


function PrivateNamespace:get(var)
	if not self.Metadata or tablelength(self.Metadata) == 0 then self:log(1, true, "You must initialize a new mod config instance before you can use the ConfigLib") return nil end
	
	if not self.ModConfig or not self.ModConfig[var] then
		self:log(1, true, "Could not load var", var, "for mod", self.Metadata.title, "- return 0")
		return 0
	end
	
	return self.ModConfig[var]["value"]
end


function PrivateNamespace:getLogPrefix(isSelf)
	isSelf = isSelf or false
	
	-- Removed date from logs, vanilla game finally readded: "[" .. os.date("%Y-%m-%d %X") .. "]"
	
	if isSelf and self.SelfMetadata and tablelength(self.SelfMetadata) > 0 then -- Return self prefix
		if not self.SelfLogPrefix or string.len(self.SelfLogPrefix) == 0 then
			self.SelfLogPrefix = "[" .. self.SelfMetadata.name .. " - " .. self.SelfMetadata.version .. "]"
		end
		return self.SelfLogPrefix
	elseif isSelf == false and self.Metadata and tablelength(self.Metadata) > 0 then -- Return calling mods prefix
		if self.logPrefix == "" then
			self.logPrefix = "[" .. self.Metadata.name .. " - " .. self.Metadata.version .. "]"
		end
		return self.logPrefix
	end
	
	return "[CONFIG LIB]" -- Fallback if metadata are not loaded yet
end

function PrivateNamespace:log(logLevel, isSelf, var, ...)
	if type(logLevel) ~= "number" then -- You forgot to add loglevel as first parameter
		self:log(1, true, "Syntax error: MyModConfig.log() expects numeric loglevel as first parameter. Skipping!")
		return
	end
	
	if logLevel <= self.logLevel then
		if type(var) == "table" then
			print(self:getLogPrefix(isSelf), ...)
            printTable(var)
        else
			if var == nil then var = "nil" end
			print(self:getLogPrefix(isSelf), var, ...)
		end
	end
end

function PrivateNamespace:setLoglevel(logLevel)
	self.logLevel = tonumber(logLevel) or self.logLevel
end

-------------------------------
-- Public namespace -----------
-------------------------------
setmetatable(ConfigLib, {__call = function(_, ...) return ConfigLib.initialize(...) end}) -- lazy initialization
ConfigLib.CreateInstance = setmetatable({new = new}, {__call = function(_, ...) return new(...) end})


-- Initialize a new mod config instance
-- Usage:
-- local MyModConfig = ConfigLib("modID")
function ConfigLib.initialize(modID)
	local instance = {}
	local lib = ConfigLib.CreateInstance()
	
	if not lib.SelfMetadata then
		self:log(1, true, "Could not get self mod id!")
	end
	
	if not modID or type(modID) ~= "string" then
		self:log(1, true, "Could not initialize mod. Mod ID must be type of string and not be empty.", type(modID), "given")
		return {}
	end
	
	-- Load metadata
	lib.Metadata = lib:getMetadata(modID) -- Load the mods metadata
	
	-- Load mod config
	if lib.Metadata and tablelength(lib.Metadata) > 0 then
		local modName = lib.Metadata.name
		
		lib.ModConfig = lib:loadModConfig(modID) -- Load mods config file
		lib:log(3, true, "Load mod:", lib.Metadata.name)
	end
	
	
	instance.get               = function(...) return lib:get(...) end
	instance.log               = function(loglevel, ...) return lib:log(loglevel, false, ...) end
	instance.getLoglevel       = function() return lib.logLevel end
	instance.setLoglevel       = function(loglevel) return lib:setLoglevel(loglevel) end
	
	
	return instance
end



return ConfigLib

