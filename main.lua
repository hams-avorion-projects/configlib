package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"
package.path = package.path .. ";data/scripts/player/?.lua"
package.path = package.path .. ";data/scripts/?.lua"


local ConfigIO = include("ConfigIO")
local mods = include("ConfigLibRegistration")

function initialize()
	-- Create / update mod configs on server
	createDirectory(Server().folder .. "/moddata/ConfigLib")
	
	
	for _,modID in pairs(mods) do
		ConfigIO.updateModConfig(modID)
	end
	
    Server():registerCallback("onPlayerLogIn", "onPlayerLogIn")
end


function onPlayerLogIn(playerIndex)
	local player = Player(playerIndex)
	
	player:addScriptOnce("ConfigLoader.lua") -- Helper functions to download configs from Server
end



